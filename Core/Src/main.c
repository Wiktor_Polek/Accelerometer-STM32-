/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "lsm6dso.h"
#include "stm32f4xx_nucleo_bus.h"
#include <math.h>
#include <stdio.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
UART_HandleTypeDef huart2;
UART_HandleTypeDef huart3;

/* USER CODE BEGIN PV */
LSM6DSO_Object_t acc_object;	//glowna struktura akcelerometru
LSM6DSO_IO_t IO_acc;	// struktura przechowujaca funkcje do obslugi akcelerometru
LSM6DSO_Axes_t acc_axes;	//struktura przechowujaca wartosci przyspieszen
LSM6DSO_Capabilities_t acc_capa; //struktura przechowujaca ustawienia czujnika
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_UART4_Init(void);
static void MX_USART3_UART_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init(); //inicjacja perferiów GPIO
  MX_USART2_UART_Init();	//inicjacja portu USART2 do kommnikacji z PC
  MX_USART3_UART_Init();	//inicjacja portu USART3 do komunikacji z drugim uC
  /* USER CODE BEGIN 2 */

  // Only for PC UART communication:

  	  uint8_t data[400]; // Tablica przechowujaca wysylana wiadomosc
	  uint16_t size = 0; // Rozmiar wysylanej wiadomosci

	  size = sprintf(data, "Init");
	  HAL_UART_Transmit_IT(&huart2, data, size);


	  //ACC_configuration -
	  IO_acc.Init = BSP_I2C1_Init;		//okreslenie funkcji inicjujacej czujnik
	  IO_acc.Address = 0xD6; 			//okreslenie adresu urzadzenia
	  IO_acc.BusType = LSM6DSO_I2C_BUS;	//okreslenie typu magistrali
	  IO_acc.GetTick = HAL_GetTick();	//okreslenie funkcji ktora zwraca aktualny czas
	  IO_acc.DeInit = BSP_I2C1_DeInit;	//okreslenie funkcji deinicjującej czujnik
	  IO_acc.ReadReg = BSP_I2C1_ReadReg;	//okreslenie funkcji do odczytu danych z magistrali I2C
	  IO_acc.WriteReg = BSP_I2C1_WriteReg;	//okreslenie funkcji do zapisu danych do magistrali I2C

	  LSM6DSO_RegisterBusIO(&acc_object, &IO_acc); //inicjacja rejestrowania wejsc/wyjsc na
	  LSM6DSO_COMMON_Driver.Init(&acc_object); //inicjacja drivera od akcelerometru
	  LSM6DSO_COMMON_Driver.GetCapabilities(&acc_object, &acc_capa);
	  LSM6DSO_ACC_Driver.Enable(&acc_object);	//załączenie drivera od akcelerometru

	  int32_t Scale = 4;	//wybranie skali pomiarowej +- 4g (2,4,8,16)
	  LSM6DSO_ACC_SetFullScale(&acc_object, Scale); //wgranie do akcelerometru ustawien o  skali

	  struct acc_struct //struktura przechowujaca dane pomiarowe, koniecznosc uzycia struktury ze zgledu na sposob wyslania
	  {
	  float acc_x;
	  float acc_y;
	  float acc_z;
	  float angle_x;
	  float angle_y;
	  float angle_z;
	  };
	  struct acc_struct acc_val;
	  const float pi = 3.141592;

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  if(LSM6DSO_ACC_Driver.GetAxes(&acc_object, &acc_axes) == LSM6DSO_OK){
		  //funkcja w ifie pobiera dane z akcelerometru o aktualnych przyspieszeniach

		  //przeliczenie wartosci przyspieszen z uwzglednieniem skali
		  acc_val.acc_x = acc_axes.x/Scale*2;
		  acc_val.acc_y = acc_axes.y/Scale*2;
		  acc_val.acc_z = acc_axes.z/Scale*2;

		  //wyliczenie katow przechylu
		  acc_val.angle_x = atan2(acc_val.acc_x, sqrt(pow(acc_val.acc_y,2) + pow(acc_val.acc_z,2)))*(180/pi);
		  acc_val.angle_y = atan2(acc_val.acc_y, sqrt(pow(acc_val.acc_x,2) + pow(acc_val.acc_z,2)))*(180/pi);
		  acc_val.angle_z = atan2(sqrt(pow(acc_val.acc_x,2) + pow(acc_val.acc_y,2)), acc_val.acc_z)*(180/pi);

		  acc_val.acc_x = acc_val.acc_x/100;
		  acc_val.acc_y = acc_val.acc_y/100;
		  acc_val.acc_z = acc_val.acc_z/100;

		  /*
		   * Send to device - STM32 F746G - bit mode
		   */
		  HAL_UART_Transmit_IT(&huart3, &acc_val, sizeof(acc_val));
		  HAL_Delay(50);

		  /*
		   * Send to PC - string mode
		   */

		  size = sprintf(data, "X:%f Y: %f Z: %f \n\r", acc_val.acc_x,acc_val.acc_y,acc_val.acc_z);
		  HAL_UART_Transmit_IT(&huart2, data, size);
		  HAL_Delay(50);
		  size = sprintf(data, "kat w osi X:%.2f kat w osi Y:%.2f kat w osi Z:%.2f  \n\r", acc_val.angle_x,acc_val.angle_y, acc_val.angle_z);
		  HAL_UART_Transmit_IT(&huart2, data, size);
		  HAL_Delay(50);

	  }
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  RCC_OscInitStruct.PLL.PLLR = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 9600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART3_UART_Init(void)
{

  /* USER CODE BEGIN USART3_Init 0 */

  /* USER CODE END USART3_Init 0 */

  /* USER CODE BEGIN USART3_Init 1 */

  /* USER CODE END USART3_Init 1 */
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 9600;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART3_Init 2 */

  /* USER CODE END USART3_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LD2_Pin */
  GPIO_InitStruct.Pin = LD2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD2_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
